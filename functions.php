<?php
require __DIR__ . '/vendor/autoload.php';

function rm_tmp($dir)
{
    global $logger;
    if (file_exists($dir)) {
        if (!$dh = @opendir($dir)) return;
        while ($obj = readdir($dh)) {
            if ($obj == '.' || $obj == '..') continue;
            if (unlink($dir . '/' . $obj))
                $logger->debug("$dir/$obj removed");
        }
        closedir($dh);
        rmdir($dir);
    }
    return;
}

// funzione usata per cancellare i backup storici
function rm_bkp($dir, $seconds)
{
    global $logger;
    if (!$dh = @opendir($dir)) return;
    while ($obj = readdir($dh)) {
        if ($obj == '.' || $obj == '..') continue;
        if ((time() - filemtime($dir . "/" . $obj)) > $seconds) {
            if (unlink($dir . '/' . $obj))
                $logger->debug("$dir/$obj removed");
        }
    }
    closedir($dh);
    return;
}

// funzione usata per cancellare i backup storici
function rm_bkp_from_GDrive($dir, $seconds, $service)
{
    global $logger;

    $logger->debug("opening '$dir' and removing old files > $seconds s");
    if (!$dh = @opendir($dir)) return;
    while ($obj = readdir($dh)) {
        if ($obj == '.' || $obj == '..') continue;
        if ((time() - filemtime($dir . "/" . $obj)) > $seconds) {
            $service->files->delete($obj);
            if (unlink($dir . '/' . $obj))
                $logger->debug("$dir/$obj removed");
        }
    }
    closedir($dh);
    return;
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    global $CONF;
    $client = new Google_Client();
    $client->setApplicationName('Google Drive API PHP');
    $client->setScopes(Google_Service_Drive::DRIVE);
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    $tokenPath = $CONF['gapi_token_path'];
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

function closeAndExit(){
    global $message,$CONF;
    if ($CONF['log_smtp_enabled'] == true)
        $message->setSubject('[WANDA - Bkp] Errore');
    exit;
}