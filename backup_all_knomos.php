<?php
require_once "conf.php";
require_once "functions.php";

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\SwiftMailerHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Formatter\HtmlFormatter;
use Monolog\Handler\BufferHandler;

$logger = new Logger('WANDA Backup');
$formatter = new LineFormatter();
$formatter->ignoreEmptyContextAndExtra(true);
$rotatingFileHandler = new RotatingFileHandler($CONF['log_path'], $CONF['log_max_days'], Logger::DEBUG);
$rotatingFileHandler->setFormatter($formatter);
$stdoutHanlder = new StreamHandler('php://stdout', Logger::DEBUG);
$stdoutHanlder->setFormatter($formatter);
$logger->pushHandler($rotatingFileHandler);
$logger->pushHandler($stdoutHanlder);

if ($CONF['log_smtp_enabled'] == true) {
    // Create the Transport
    $transport = (new Swift_SmtpTransport($CONF['log_smtp_host'], $CONF['log_smtp_port'], 'tls'))
        ->setUsername($CONF['log_smtp_username'])
        ->setPassword($CONF['log_smtp_password']);

    $https['ssl']['verify_peer'] = FALSE;
    $https['ssl']['verify_peer_name'] = FALSE; // seems to work fine without this line so far
    $host = gethostname();
    $ip = gethostbyname($host);

    $transport->setLocalDomain($ip);
    $transport->setStreamOptions($https);

    $mailer = new Swift_Mailer($transport);
    if ($CONF['log_smtp_debug'])
        $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin(new Swift_Plugins_Loggers_EchoLogger()));

    // Create a message
    $message = (new Swift_Message('[WANDA - Bkp] OK procedura backup giornaliera'))
        ->setFrom($CONF['log_smtp_from'])
        ->setTo($CONF['log_smtp_to'])
        ->setBody('', 'text/html');

    $swiftMailHandler = new SwiftMailerHandler($mailer, $message, Logger::DEBUG);
    $htmlFormatter = new HtmlFormatter();
    $swiftMailHandler->setFormatter($htmlFormatter);

    $bufferHandler = new BufferHandler($swiftMailHandler);
    $logger->pushHandler($bufferHandler);
}

if (!file_exists($CONF['path_storico'])) {
    $logger->debug("creating {$CONF['path_storico']}");
    mkdir($CONF['path_storico'], 0700, true);
}

// path temporaneo
$path_tmp = $CONF['path_backup'] . "/tmp";

if (file_exists($path_tmp)) {
    $logger->debug("clean $path_tmp");
    rm_tmp($path_tmp);
}

if (mkdir($path_tmp, 0700, true))
    $logger->debug("create $path_tmp");

$mysql_link = mysqli_connect($CONF['dbserver'], $CONF['dbuser'], $CONF['dbpassword']);
if (!$mysql_link) {
    $logger->error(mysqli_connect_errno() . " " .  mysqli_connect_error());
    closeAndExit();
}

$logger->info("connected to {$CONF['dbserver']} with user {$CONF['dbuser']}");

// ottengo la lista dei database disponibili
$query = "SHOW DATABASES";
$result = mysqli_query($mysql_link, $query);

$dbsToBackupArray = array_map('strtolower', (array) $CONF['dbsArray']);
$array_db = array();
while ($row = mysqli_fetch_assoc($result)) {
    if ($row['Database'] != 'information_schema' && $row['Database'] != 'mysql' && $row['Database'] != 'performance_schema') {
        if (in_array(strtolower($row["Database"]), $dbsToBackupArray)) {
            $logger->info("dump of " . $row["Database"]);
            $lastLine = exec(
                "{$CONF['mysqldump']} --user={$CONF['dbuser']} --password={$CONF['dbpassword']} --databases $row[Database] > $path_tmp/$row[Database].sql",
                $execOutput,
                $returnCode
            );
            if ($returnCode != 0) {
                $logger->error("command error. Exit code is $returnCode.");
                closeAndExit();
            }

            foreach ($execOutput as $row) {
                $logger->info($row);
            }
            $array_db[] = $row['Database'] . ".sql";
        }
    }
}

$pharFilePath = $CONF['path_storico'] . "/" . $CONF['outFileName'] . ".tar";
$logger->debug("creating $pharFilePath");
$pharFile = new PharData($pharFilePath);
for ($d = 0; $d < count($array_db); $d++) {
    $fileToAdd =  $array_db[$d];
    $logger->debug("adding $fileToAdd");
    $pharFile->addFile($path_tmp . "/" . $array_db[$d], $fileToAdd);
}
$logger->info("Adding {$CONF['knomosPath']}");
$pharFile->buildFromDirectory($CONF['knomosPath']);
$pharFile->compress(Phar::GZ);
$logger->info("Compressed to GZ");
unset($pharFile);

if (unlink($pharFilePath))
    $logger->info("removed $pharFilePath");

$logger->info("removing $path_tmp");
rm_tmp($path_tmp);

$logger->info("connecting to Google API");
$client = getClient();
$service = new Google_Service_Drive($client);
$logger->info("connected to Google API");

$filesToUpload = scandir($CONF['path_storico']);
foreach ($filesToUpload as $key => $value) {

    if (!in_array($value, array(".", ".."))) {
        if (!is_dir($value)) {
            $fileMetadata = new Google_Service_Drive_DriveFile(array('name' => $value));
            //Set the Parent Folder
            if (!empty($CONF['gapi_drive_parent_folder'])) {
                $fileMetadata->setParents(array($CONF['gapi_drive_parent_folder']));
            }
            $fileToUploadPath = $CONF['path_storico'] . "/" . $value;
            $content = file_get_contents($fileToUploadPath);
            $file = $service->files->create(
                $fileMetadata,
                array(
                    'data' => $content,
                    'uploadType' => 'multipart',
                    'fields' => 'id'
                )
            );
            $logger->info("uploaded $value");
            if (!file_exists($CONF['path_backup'] . "/saved/")) {
                mkdir($CONF['path_backup'] . "/saved/");
            }
            $traceOfSaved = $CONF['path_backup'] . "/saved/" . $file->id;
            if (touch($traceOfSaved))
                $logger->info("saved $traceOfSaved");
            if (unlink($fileToUploadPath))
                $logger->info("removed $fileToUploadPath");
        }
    }
}
rm_bkp_from_GDrive($CONF['path_backup'] . "/saved", $CONF['oldFileSeconds'], $service);

$logger->info("Backup ultimato");
