<?php
date_default_timezone_set("Europe/Rome");
ini_set("memory_limit", "-1");

$CONF['outFileName']="WANDA_BACKUP_".date("YmdHis");
$CONF['path_backup']="C:/BACKUP";
$CONF['path_storico']=$CONF['path_backup']."/storico";

// connessione al database
$CONF['dbserver'] = "localhost";
$CONF['dbuser'] = "root";
$CONF['dbpassword'] = "password";
$CONF['dbsArray'] = array(
    "knomos_db",
    "knomos_db_history",
);

$CONF['mysqldump'] = '"C:\Program Files\MySQL\MySQL Server 8.0\bin\mysqldump.exe"';
$CONF['knomosPath'] = "c:/Apache24/htdocs/knomosplus";
//7giorni
$CONF['oldFileSeconds'] = 0;// 604800;

$CONF['log_path']=__DIR__.'logs/knomos_bkp.log';
$CONF['log_max_days']=7;
$CONF['log_smtp_host']="smtp.gmail.com";
$CONF['log_smtp_port']=587;
$CONF['log_smtp_username']="xxxxxxx@gmail.com";
$CONF['log_smtp_password']='xxxxxxx';
$CONF['log_smtp_debug']=false;
$CONF['log_smtp_to']= ['xxxxxxxxx@libero.it' => 'Davide Martu'];
$CONF['log_smtp_from']=['xxxxxxxxx@gmail.com' => 'Backup Logger'];

$CONF['gapi_token_path'] = __DIR__.'/token.json';